#include			<iostream>
#include			"GraphicMonitor.hh"

Nibbler::GraphicMonitor::GraphicMonitor(const int &x, const int &y)
{
  _screen = new sf::RenderWindow(sf::VideoMode(x, y, 32), "nibbler");
}

void				Nibbler::GraphicMonitor::stop()
{
  if (_screen)
    delete _screen;
}

bool				Nibbler::GraphicMonitor::isOpened() const
{
  if (_screen->IsOpened())
    return true;
  return false;
}

direction			Nibbler::GraphicMonitor::getEvent()
{
  sf::Event			Event;

  while (_screen->GetEvent(Event))
    {
      if (Event.Type ==  sf::Event::KeyPressed)
	{
	  if (Event.Key.Code == sf::Key::Left)
	    return LEFT;
	  if (Event.Key.Code == sf::Key::Right)
	    return RIGHT;
	  if (Event.Key.Code == sf::Key::Up)
	    return UP;
	  if (Event.Key.Code == sf::Key::Down)
	    return DOWN;
	  if (Event.Type == sf::Event::Closed)
	    _screen->Close();
	  if (Event.Key.Code == sf::Key::Escape)
	    _screen->Close();
	}
    }
  return NONE;
}

void				Nibbler::GraphicMonitor::display(std::list<std::pair<int, int> > &body, const std::pair<int, int> &pos) const
{
  sf::Shape	rectangle;
  _screen->Clear();
  for (std::list<std::pair<int, int> >::iterator it=body.begin(); it != body.end(); ++it)
    {
      if (*it == body.front())
	rectangle = sf::Shape::Rectangle(it->first, it->second, it->first + 10, it->second + 10, sf::Color(125, 125, 255));
      else
	rectangle = sf::Shape::Rectangle(it->first, it->second, it->first + 10, it->second + 10, sf::Color(0 ,255, 0));
      _screen->Draw(rectangle);
    }
  rectangle = sf::Shape::Rectangle(pos.first, pos.second, pos.first + 10, pos.second + 10, sf::Color(255, 0, 0));
  _screen->Draw(rectangle);
  _screen->Display();
}

extern "C"
{
  Nibbler::IDisplayModule*	createView(const int &x, const int &y)
  {
    return new			Nibbler::GraphicMonitor(x, y);
  }
}
