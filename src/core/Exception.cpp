#include	<exception>
#include	"Exception.hh"

Nibbler::Exception::Exception(std::string const& message) throw()
  :  _errorMessage(message)
{
}

Nibbler::Exception::~Exception() throw()
{
}

const char*	Nibbler::Exception::what() const throw()
{
  return ((std::string("Error: ") + this->_errorMessage).c_str());
}
