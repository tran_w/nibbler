#include			<iostream>
#include			<cstdlib>
#include			<unistd.h>
#include			"DLLoader.hh"
#include			"Snake.hh"
#include			"Food.hh"
#include			"Exception.hh"

namespace
{
  bool				checkArgs(int nb, char **args)
  {
    int				i;
    int				j;

    if (nb != 4)
      return false;
    for (j = 0; j < 2; ++j)
      {
	for (i = 0; args[j][i] != '\0'; ++i)
	  if (isdigit(args[j][i] == 0))
	    return false;
      }
    if (std::atoi(args[1]) < MINSIZE || std::atoi(args[2]) < MINSIZE)
      throw Nibbler::Exception("Size too small : Width >= 300 && Height >= 300");
    if (std::atoi(args[1]) > MAXSIZEX || std::atoi(args[2]) > MAXSIZEY)
      throw Nibbler::Exception("Size too large : Width <= 1280 && Height <= 960");
    return true;
  }

  void				nibbler(int xwin, int ywin, char *filename)
  {
    direction			dir;
    int				speed = 100000;
    std::list<std::pair<int, int> > body;
    std::pair<int, int>		food;
    Nibbler::DLLoader		*Loader = new Nibbler::DLLoader(filename);

    Loader->doOpen();

    Nibbler::IDisplayModule	*DisplayModule = Loader->getInstance(xwin, ywin);
    Nibbler::Snake		*Snake = new Nibbler::Snake(xwin/9);
    Nibbler::Food		*Food = new Nibbler::Food(xwin, ywin);

    while (DisplayModule->isOpened() == true && Snake->isAlive(xwin, ywin) == true)
      {
	food = Food->getPos();
	body = Snake->getBody();
	if ((dir = DisplayModule->getEvent()) != NONE)
	  Snake->setDir(dir);
	DisplayModule->display(body, food);
	Snake->move();
	if (Food->isEaten(body.front()) == true)
	  {
	    speed -= 1000;
	    Food->setPosition(body, xwin, ywin);
	    Snake->eatFood();
	  }
	usleep(speed);
      }
    DisplayModule->stop();
    Loader->doClose();
  }
}

int				main(int ac, char **av)
{

  try
    {
      if (checkArgs(ac, av) == false)
	throw Nibbler::Exception("Usage: ./nibbler <width> <heigth> <lib_path>");
      nibbler(std::atoi(av[1]), std::atoi(av[2]), av[3]);
    }
  catch (const Nibbler::Exception &e)
    {
      std::cerr << e.what() << std::endl;
      return false;
    }
  return true;
}
