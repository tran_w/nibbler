#include		<dlfcn.h>
#include		<iostream>
#include		"DLLoader.hh"

Nibbler::DLLoader::DLLoader(const char *filename) : _filename(filename)
{
}

Nibbler::DLLoader::~DLLoader()
{
}

void			Nibbler::DLLoader::doOpen()
{
  if ((_handle = dlopen(_filename, RTLD_LAZY)) == NULL)
    {
      std::cerr << dlerror() << std::endl;
      throw Nibbler::Exception("Failed to open the library");
    }
}

void			*Nibbler::DLLoader::getSymbol(const char *symbol)
{
  char			*error;
  void			*ptr;

  dlerror();
  ptr = dlsym(_handle, symbol);
  if ((error = dlerror()) != NULL)
    throw Nibbler::Exception(error);
  return (ptr);
}

void			Nibbler::DLLoader::doClose() const
{
  if (dlclose(_handle) != 0)
    throw Nibbler::Exception("Failed to close library");
}

Nibbler::IDisplayModule	*Nibbler::DLLoader::getInstance(const int &x, const int &y)
{
  Nibbler::IDisplayModule*		(*external_creator)(const int &, const int &);

  external_creator = reinterpret_cast<Nibbler::IDisplayModule* (*)(const int &, const int &)>(getSymbol("createView"));
  return (external_creator(x, y));
}
