#include		<iostream>
#include		<ctime>
#include		<cstdlib>
#include		"Food.hh"

Nibbler::Food::Food(const int &xwin, const int &ywin)
{
  srand(time(NULL));
  _pos.first = (rand() % xwin) / 10;
  _pos.second = (rand() % ywin) / 10;
  _pos.first *= 10;
  _pos.second *= 10;
}

bool			Nibbler::Food::isEaten(const std::pair<int, int> &head) const
{
  if (head.first == _pos.first && head.second == _pos.second)
    return true;
  return false;
}

bool			Nibbler::Food::validPosition(std::list<std::pair<int, int> > &body) const
{
  for (std::list<std::pair<int, int> >::iterator it=body.begin(); it != body.end(); ++it)
    if (it->first != _pos.first && it->second != _pos.second)
      return true;
  return false;

}

void			Nibbler::Food::setPosition(std::list<std::pair<int, int> > &body, const int &xwin, const int &ywin)
{
  do
    {
      _pos.first = (rand() % xwin) / 10;
      _pos.second = (rand() % ywin) / 10;
      _pos.first *= 10;
      _pos.second *= 10;
    }
  while (validPosition(body) == false);
}

std::pair<int, int>	Nibbler::Food::getPos() const
{
  return _pos;
}
