#include		<iostream>
#include		"Snake.hh"

Nibbler::Snake::Snake(int size) : _size(size), _direction(NONE)
{
  int		i = 0;
  int		x = 200;
  int		y = 200;

  _body.push_back(std::pair<int, int>(x, y));
  while (i < size)
    {
      x += 10;
      _body.push_back(std::pair<int, int>(x, y));
      i += 10;
    }
}

Nibbler::Snake::~Snake() {}

void			Nibbler::Snake::setDir(const direction &dir)
{
  if (((_direction == UP || _direction == DOWN) && (dir == UP || dir == DOWN)) || ((_direction == RIGHT || _direction == LEFT) && (dir == RIGHT || dir == LEFT)) || (_direction == NONE && dir == RIGHT))
    return ;
  _direction = dir;
}

void			Nibbler::Snake::move()
{
  switch (_direction)
    {
    case UP:
      _body.push_front(std::pair<int, int>(_body.front().first, _body.front().second - 10));
      break;
    case DOWN:
      _body.push_front(std::pair<int, int>(_body.front().first, _body.front().second + 10));
      break;
    case LEFT:
      _body.push_front(std::pair<int, int>(_body.front().first - 10, _body.front().second));
      break;
    case RIGHT:
      _body.push_front(std::pair<int, int>(_body.front().first + 10, _body.front().second));
      break;
    default:
      return ;
    }
  _body.pop_back();
}

std::list<std::pair<int, int> >	Nibbler::Snake::getBody() const
{
  return _body;
}

int			Nibbler::Snake::getSize() const
{
  return _size;
}

void			Nibbler::Snake::eatFood()
{
  if (_direction == RIGHT)
    _body.push_back(std::pair<int, int>(_body.back().first + 10, _body.back().second));
  if (_direction == LEFT)
    _body.push_back(std::pair<int, int>(_body.back().first - 10, _body.back().second));
  if (_direction == UP)
    _body.push_back(std::pair<int, int>(_body.back().first, _body.back().second + 10));
  if (_direction == LEFT)
    _body.push_back(std::pair<int, int>(_body.back().first, _body.back().second - 10));
}

bool			Nibbler::Snake::isAlive(const int &xwin, const int &ywin)
{
  int			first = 0;

  for (std::list<std::pair<int, int> >::iterator it=_body.begin(); it != _body.end(); ++it)
    if (_body.front().first == it->first && _body.front().second == it->second && first++ != 0)
      return false;
  if (_body.front().first < 0 || _body.front().second < 0 || _body.front().first >= xwin || _body.front().second >= ywin)
    return false;
  return true;
}
