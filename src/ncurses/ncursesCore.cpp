#include					<iostream>
#include					"ncursesCore.hh"

Nibbler::ncursesCore::ncursesCore(const int &x, const int &y)
{
  int						currentHeight;
  int						currentWidth;

  initscr();
  this->_isAlive = true;
  getmaxyx(stdscr, currentHeight, currentWidth);
  if ((currentWidth < x / X_SCALE + HEADER_X_SIZE) || (currentHeight < y / Y_SCALE + HEADER_Y_SIZE))
    {
      printw("Your terminal is too small. Enlarge it! Press <q> to Quit");
      refresh();
      if (getch() == 'q')
	this->_isAlive = false;
    }
  keypad(stdscr, TRUE);
  noecho();
  curs_set(0);
  nodelay(stdscr, TRUE);
  this->_win = new Nibbler::ncursesWindowsManager(y / X_SCALE + 1, (x + Y_SCALE) / Y_SCALE, 5, 1);
  this->_win->initColor();
  this->_win->createBox();
  mvprintw(0, 2, "Press the following keys to move the snake:");
  mvprintw(1, 2, "<UP> to move up, <DOWN> to move down,");
  mvprintw(2, 2, "<LEFT> to move left, <RIGHT> to move right.");
  mvprintw(3, 2, "Press <q> or <ESC> two times to quit the game");
}

void						Nibbler::ncursesCore::stop()
{
  endwin();
}

bool						Nibbler::ncursesCore::isOpened() const
{
  if (this->_isAlive == true)
    return true;
  return false;
}

direction			       		Nibbler::ncursesCore::getEvent()
{
  int						input;

  input = getch();
  switch (input)
    {
    case KEY_UP:
      return (UP);
    case KEY_DOWN:
      return (DOWN);
    case KEY_LEFT:
      return (LEFT);
    case KEY_RIGHT:
      return (RIGHT);
    case 'q':
    case ESC_KEY:
      this->setAlive(false);
      break;
    }
  return NONE;
}

void						Nibbler::ncursesCore::display(std::list<std::pair<int, int> > &body,
							      const std::pair<int, int> &food) const
{
  std::list<std::pair<int, int> >::iterator	it;

  this->_win->clear();
  this->_win->attrColorOn(Nibbler::CYAN);
  this->_win->createBox();
  mvwprintw(this->_win->getWindow(), 0, 1, GAME_NAME);
  this->_win->attrColorOff(Nibbler::CYAN);
  for (it = body.begin(); it != body.end(); ++it)
    {
      if (*it == body.front())
	{
	  this->_win->attrColorOn(GREEN);
	  mvwaddch(this->_win->getWindow(), it->second / X_SCALE, it->first / Y_SCALE, '0');
	  this->_win->attrColorOff(GREEN);
	}
      else
	mvwaddch(this->_win->getWindow(), it->second / X_SCALE, it->first / Y_SCALE, 'O');
    }
  this->_win->attrColorOn(Nibbler::RED);
  mvwaddch(this->_win->getWindow(), food.second / X_SCALE, food.first / Y_SCALE, '#');
  this->_win->attrColorOn(Nibbler::RED);
  this->_win->refresh();
  refresh();
}

void						Nibbler::ncursesCore::setAlive(bool a)
{
  this->_isAlive = a;
}

extern "C"
{
  Nibbler::IDisplayModule*			createView(const int &x, const int &y)
  {
    return new					Nibbler::ncursesCore(x, y);
  }
}
