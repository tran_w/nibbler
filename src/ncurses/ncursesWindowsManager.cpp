#include			"ncursesWindowsManager.hh"

Nibbler::ncursesWindowsManager::ncursesWindowsManager(const int &x, const int &y, const int &startX, const int &startY) :
  _window(newwin(x, y, startX, startY))
{
}

Nibbler::ncursesWindowsManager::ncursesWindowsManager(const Nibbler::ncursesWindowsManager &k) :
  _window(k._window)
{
}

Nibbler::ncursesWindowsManager::~ncursesWindowsManager()
{
}

Nibbler::ncursesWindowsManager	&Nibbler::ncursesWindowsManager::operator=(const Nibbler::ncursesWindowsManager &k)
{
  if (this != &k)
    {
      _window = k._window;
    }
  return *this;
}

void				Nibbler::ncursesWindowsManager::createBox() const
{
  box(this->_window, 0, 0);
}

WINDOW				*Nibbler::ncursesWindowsManager::getWindow() const
{
  return (this->_window);
}

void				Nibbler::ncursesWindowsManager::initColor() const
{
  int				backgroundColor;

  start_color();
  if (use_default_colors () == ERR)
    backgroundColor = COLOR_BLACK;
  else
    backgroundColor = -1;
  init_pair(GREEN, COLOR_GREEN, backgroundColor);
  init_pair(CYAN, COLOR_CYAN, backgroundColor);
  init_pair(RED, COLOR_RED, backgroundColor);
  init_pair(BLACK, COLOR_BLACK, backgroundColor);
}

void				Nibbler::ncursesWindowsManager::attrColorOn(const attrColor colorName) const
{
  wattron(this->_window, COLOR_PAIR(colorName));
}

void				Nibbler::ncursesWindowsManager::attrColorOff(const attrColor colorName) const
{
  wattroff(this->_window, COLOR_PAIR(colorName));
}

void				Nibbler::ncursesWindowsManager::clear() const
{
  wclear(this->_window);
}

void				Nibbler::ncursesWindowsManager::refresh() const
{
  wrefresh(this->_window);
}
