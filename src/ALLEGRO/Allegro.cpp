#include			<iostream>
#include			"Allegro.hh"

Nibbler::Allegro::Allegro(const int &x, const int &y)
{
  _state = true;
  if(!al_init() || (_screen = al_create_display(x, y)) == NULL)
    _state = false;
  if ((_event_queue = al_create_event_queue()) == NULL)
    {
      al_destroy_display(_screen);
      _state = false;
    }
  al_init_primitives_addon();
  al_init_image_addon();
  corps = al_load_bitmap("./resources/corps.bmp");
  tete = al_load_bitmap("./resources/tete.bmp");
  pomme = al_load_bitmap("./resources/pomme.bmp");
  al_register_event_source(_event_queue, al_get_display_event_source(_screen));
  al_install_keyboard();
  al_register_event_source(_event_queue, al_get_keyboard_event_source());
}

bool				Nibbler::Allegro::isOpened() const
{
  return _state;
}

direction			Nibbler::Allegro::getEvent()
{
  ALLEGRO_EVENT			ev;
  ALLEGRO_TIMEOUT		timeout;

  al_init_timeout(&timeout, 0.01);
  al_wait_for_event_until(_event_queue, &ev, &timeout);
  al_flush_event_queue(_event_queue);
  if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
    _state = false;
  if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
    {
      if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
	_state = false;
      if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN || ev.keyboard.keycode == ALLEGRO_KEY_S)
	return DOWN;
      if (ev.keyboard.keycode == ALLEGRO_KEY_UP || ev.keyboard.keycode == ALLEGRO_KEY_W || ev.keyboard.keycode == ALLEGRO_KEY_Z)
	return UP;
      if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT || ev.keyboard.keycode == ALLEGRO_KEY_A || ev.keyboard.keycode == ALLEGRO_KEY_Q)
	return LEFT;
      if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT || ev.keyboard.keycode == ALLEGRO_KEY_D)
	return RIGHT;
    }
  return NONE;
}

void				Nibbler::Allegro::display(std::list<std::pair<int, int> > &body, const std::pair<int, int> &pos) const
{
  al_clear_to_color(al_map_rgb(0, 0, 0));
  for (std::list<std::pair<int, int> >::iterator it=body.begin(); it != body.end(); ++it)
    {
      if (*it == body.front())
	al_draw_bitmap(tete, it->first, it->second, ALLEGRO_FLIP_HORIZONTAL);
      else
	al_draw_bitmap(corps, it->first, it->second, ALLEGRO_FLIP_HORIZONTAL);
    }
  al_draw_bitmap(pomme, pos.first, pos.second, 0);
  al_flip_display();
}

extern "C"
{
  Nibbler::IDisplayModule*	createView(const int &x, const int &y)
  {
    return new			Nibbler::Allegro(x, y);
  }
}
