NAME_EXE	=	.nibbler
TMP		=	nibbler

PATH_LIB	=	./lib/
PATH_CORE	=	./src/core/

# Core
SRCS_EXE	=	$(PATH_CORE)main.cpp	\
	  		$(PATH_CORE)DLLoader.cpp \
	  		$(PATH_CORE)Exception.cpp \
			$(PATH_CORE)Snake.cpp \
			$(PATH_CORE)Food.cpp
OBJS_EXE	=	$(SRCS_EXE:.cpp=.o)

# ALLEGRO
ALLEGRO		=	lib_$(TMP)_allegro.so
ALLEGRO_SRCS	=	src/ALLEGRO/Allegro.cpp
ALLEGRO_OBJS	=	$(ALLEGRO_SRCS:.cpp=.o)

# SFML
SFML		=	lib_$(TMP)_sfml.so
SFML_SRCS	=	src/SFML/GraphicMonitor.cpp
SFML_OBJS	=	$(SFML_SRCS:.cpp=.o)

# nCurses
NCURSES		=	lib_$(TMP)_ncurses.so
NCURSES_SRCS	=	src/ncurses/ncursesCore.cpp \
			src/ncurses/ncursesWindowsManager.cpp
NCURSES_OBJS	=	$(NCURSES_SRCS:.cpp=.o)

LDFLAGS		+=	-ldl -lallegro -lncurses
LDFLAGS		+=	./SFML-1.6/lib/libsfml-graphics.so.1.6
LDFLAGS		+=	./SFML-1.6/lib/libsfml-window.so.1.6
LDFLAGS		+=	./SFML-1.6/lib/libsfml-system.so.1.6
CXXFLAGS	+=	-fPIC -W -Wall -I ./inc -I SFML-1.6/include/ -I/usr/include/allegro5

CXX		=	g++

all		:	$(ALLEGRO) $(SFML) $(NCURSES) $(NAME_EXE)

$(NAME_EXE)	:	$(OBJS_EXE)
			$(CXX) -o $(NAME_EXE) $(OBJS_EXE) $(LDFLAGS)
			@(echo 'LD_LIBRARY_PATH=./SFML-1.6/lib:/usr/local/lib ./.nibbler $$1 $$2 $$3 $$4' > $(TMP))
			@chmod +x $(TMP)
			@echo "The compilation of the program has been successful"

$(ALLEGRO)		:$(ALLEGRO_OBJS)
			$(CXX) -shared -o $(PATH_LIB)$(ALLEGRO) $(ALLEGRO_OBJS) -lallegro -lallegro_primitives -lallegro_image

$(SFML)		:	$(SFML_OBJS)
			$(CXX) -shared -o $(PATH_LIB)$(SFML) $(SFML_OBJS)

$(NCURSES)	:	$(NCURSES_OBJS)
			$(CXX) -shared -o $(PATH_LIB)$(NCURSES) $(NCURSES_OBJS)

clean		:
			$(RM) $(ALLEGRO_OBJS)
			$(RM) $(SFML_OBJS)
			$(RM) $(NCURSES_OBJS)
			$(RM) $(OBJS_EXE)

fclean		:	clean
			$(RM) $(TMP)
			$(RM) $(NAME_EXE)
			$(RM) $(PATH_LIB)$(ALLEGRO)
			$(RM) $(PATH_LIB)$(SFML)
			$(RM) $(PATH_LIB)$(NCURSES)

re		:	fclean all
