# Nibbler - EPITECH PROMOTION 2016

## What is Nibbler?
*Nibbler is a video game in which the player controls a snake moving within a plan. The window edges are the limits of the plan. If the snake hits one of these edges, or hits a part of his own body, the game is over.*

## Requirements
The following requirements should be met to run the program correctly:

* SFML

* Allegro

* ncurses

* Linux

## Installation
To install:

**make**

## Usage
Write the following command in your terminal to launch the program

**./nibbler <width> <height> <library_path>**

## How to play?
Here are the following keys to play Nibbler:
* <UP> to turn up,
* <DOWN> to turn down,
* <LEFT> to turn left,
* <RIGHT> to turn right.
* <ESC> or <q> to quit the game.

## About
This project is realized in C++ language.

## Credits
PETITPRE	Guillaume <guillaume.petitpre@epitech.eu>  
NAINA		Richard <richard.naina@epitech.eu>  
TRAN		Duy-Laurent <duy-laurent.tran@epitech.eu>  
