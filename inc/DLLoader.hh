#ifndef			DLLOADER_HH_
# define		DLLOADER_HH_

# include		"Exception.hh"
# include		"IDisplayModule.hh"


namespace		Nibbler
{
  class			DLLoader
  {
  private:
    void		*_handle;
    const char		*_filename;
  public:
    DLLoader(const char *filename);
    ~DLLoader();
    void		doOpen();
    IDisplayModule	*getInstance(const int &k, const int &j);
    void		*getSymbol(const char *symbol);
    void		doClose() const;
  };
};

#endif			/* !DLLOADER */
