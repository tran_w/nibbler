#ifndef					SNAKE_HH_
# define				SNAKE_HH_

# include				<list>

# define				MINSIZE		300
# define				MAXSIZEX	1280
# define				MAXSIZEY	960

enum direction {UP, DOWN, RIGHT, LEFT, NONE};

namespace				Nibbler
{
  class					Snake
  {
  private:
    std::list<std::pair<int, int> >	_body;
    int					_size;
    direction				_direction;
  public:
    Snake(int size);
    ~Snake();
    void				setDir(const direction &dir);
    void				move();
    void				eatFood();
    bool				isAlive(const int &xwin, const int &ywin);
    std::list<std::pair<int, int> >	getBody() const;
    int					getSize() const;
  };
};

#endif
