#ifndef			FOOD_HH_
# define		FOOD_HH_

# include		<list>

namespace		Nibbler
{
  class			Food
  {
  private:
    std::pair<int, int> _pos;
  public:
    Food(const int &xwin, const int &ywin);
    ~Food() {}
    bool		isEaten(const std::pair<int, int> &) const;
    bool		validPosition(std::list<std::pair<int, int> > &) const;
    void		setPosition(std::list<std::pair<int, int> > &, const int &, const int &);
    std::pair<int, int>	getPos() const;
  };
}

#endif			/* !FOOD_HH_ */
