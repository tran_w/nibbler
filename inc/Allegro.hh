#ifndef			ALLEGRO_HH_
# define		ALLEGRO_HH_

# include		<allegro5/allegro.h>
# include		<allegro5/allegro_primitives.h>
# include		<allegro5/allegro_image.h>
# include		"IDisplayModule.hh"

namespace		Nibbler
{
  class			Allegro : public Nibbler::IDisplayModule
  {
  public:
    Allegro(const int &x, const int &y);
    ~Allegro() {};
    virtual void	stop() {};
    virtual bool	isOpened() const;
    virtual direction	getEvent();
    virtual void	display(std::list<std::pair<int, int> > &body, const std::pair<int, int> &pos) const;
  private:
    ALLEGRO_BITMAP	*corps;
    ALLEGRO_BITMAP	*tete;
    ALLEGRO_BITMAP	*pomme;
    ALLEGRO_DISPLAY	*_screen;
    ALLEGRO_EVENT_QUEUE	*_event_queue;
    bool		_state;
  };
}

#endif			/* !ALLEGRO_HH_ */
