#ifndef					NCURSESCORE_HH_
# define				NCURSESCORE_HH_

# include				<ncurses.h>
# include				<curses.h>
# include				"ncursesWindowsManager.hh"
# include				"IDisplayModule.hh"

# define				ERR_INIT	"Could not initialise ncurses library"
# define				ERR_END		"Could not end the curses mode correctly"
# define				ERR_DISPLAY	"An error has occured at display time"
# define				ERR_EVENT	"An error has occured while catching events"

# define				GAME_NAME	"THE GAME of Nibbler"

# define				X_SCALE		(10)
# define				Y_SCALE		(10)
# define				HEADER_X_SIZE	(20)
# define				HEADER_Y_SIZE	(6)

# define				ESC_KEY		(27)

namespace				Nibbler
{
  class					ncursesCore : public Nibbler::IDisplayModule
  {
  private:
    Nibbler::ncursesWindowsManager	*_win;
    bool				_isAlive;
  public:
    ncursesCore(const int &x, const int &y);
    ~ncursesCore() {};
    virtual void       			stop();
    void				setAlive(bool a);
    virtual bool			isOpened() const;
    virtual void			display(std::list<std::pair<int, int> > &body,
						const std::pair<int, int> &pos) const;
    virtual direction			getEvent();
  };
}

#endif					/* !NCURSESCORE_HH_ */
