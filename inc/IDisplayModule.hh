#ifndef			IDISPLAYMODULE_HH_
# define		IDISPLAYMODULE_HH_

# include		"Snake.hh"

namespace		Nibbler
{
  class			IDisplayModule
  {
  public:
    virtual ~IDisplayModule() {};

    virtual void	stop() = 0;
    virtual bool	isOpened() const = 0;
    virtual direction	getEvent() = 0;
    virtual void	display(std::list<std::pair<int, int> > &body, const std::pair<int, int> &pos) const = 0;
  };
};

#endif			/* !IDISPLAYMODULE */
