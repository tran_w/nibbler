#ifndef			GRAPHICMONITOR_HH_
# define		GRAPHICMONITOR_HH_

# include		<SFML/Graphics.hpp>
# include		"IDisplayModule.hh"

namespace		Nibbler
{
  class			GraphicMonitor : public Nibbler::IDisplayModule
  {
  public:
    GraphicMonitor(const int &x, const int &y);
    ~GraphicMonitor() {};
    virtual bool       	isOpened() const;
    virtual direction  	getEvent();
    virtual void       	display(std::list<std::pair<int, int> > &body, const std::pair<int, int> &pos) const;
    virtual void       	stop();
  private:
    sf::RenderWindow	*_screen;
  };
}

#endif			/* !GRAPHICMONITOR_HH_ */
