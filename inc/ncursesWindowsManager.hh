#ifndef					NCURSESWINDOWSMANAGER_HH__
# define				NCURSESWINDOWSMANAGER_HH__

#include				<iostream>
#include				<ncurses.h>
#include				"Exception.hh"

namespace				Nibbler
{
  enum					attrColor
    {
      CYAN = 1,
      RED,
      GREEN,
      BLACK
    };
  class					ncursesWindowsManager
  {
    WINDOW    				*_window;
  public:
    ncursesWindowsManager(const int &x, const int &y, const int &startX, const int &startY);
    ncursesWindowsManager(const Nibbler::ncursesWindowsManager &k);
    ~ncursesWindowsManager();
    Nibbler::ncursesWindowsManager     	&operator=(const Nibbler::ncursesWindowsManager &k);
    void				initColor() const;
    void				attrColorOn(const attrColor colorName) const;
    void				attrColorOff(const attrColor colorName) const;
    void				createBox() const;
    void				clear() const;
    void				refresh() const;
    WINDOW				*getWindow() const;
  };
}
#endif					/* NCURSESWINDOWSMANAGER_HH__ */
